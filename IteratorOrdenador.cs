//****LIBRERIA****
using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

//PROYECTO
namespace EXPOSICION_ANTHONY
{
    public class IteradorOrdenador
    {
        private ArrayList ordenador;
        private int posicion;
        //dos clases private una con Arrraylist tipo ordenador
        //la siguiente una variable entera con la posicion
        public IteradorOrdenador(Ordenador o)
        {
            this.ordenador = o.datos;
            posicion = 0;
        }
        //el construtor se le pasa como parametro tipo ordenador
        //y se le asigna a nuestro Arraylist
        public Boolean existeSiguiente()
        {
            if (posicion < ordenador.Count)
                return true;
            else
                return false;
        }
        //metodo para validar tipi boolean para comprobar que existe el
        //siguiente en el elemento para que no continue recorriendo
        //la lista en caso de que no exista
        public Object siguiente()
        {
            object valor = ordenador[posicion];
            posicion++;
            return valor;
        }
        //el otro elemento se va obteniendo el siguiente elemento ya que este patron
        //va obteniendo de manera interactiva el siguiente elemento de la lista
    }
}
