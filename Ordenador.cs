//****LIBRERIA****
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//PROYECTO
namespace EXPOSICION_ANTHONY
{
       public class Ordenador
       {
           public ArrayList datos;
        //una clase con arrayList que va hacer nuestro contenedor de objeto
        //tipo string
           public Ordenador()
           {
                datos = new ArrayList();
           }
        //un constructor que inicializa el arraylist datos
           public Ordenador dameValor(int pos)
           {
                Ordenador o = null;
                if (pos < datos.Count)
                    return (Ordenador)datos[pos];

                return o;
           }
        //un metodo damevalor que nos dara el valor que esta en la posicion del arraylist
        //si el ordenador, si la posicion que como parametro es menor que un elemento de la
        //lista nos devuelve el elemento a la posicion en caso contrario devolvera
           public void añadirValor(string valor)
           {
                datos.Add(valor);
           }
        // los datos de tipo string
           public int dimension()
           {
                return datos.Count;
           }
        //metodo dimension que nos da el tamaño del arraylist
           public IteradorOrdenador iterador()
           {
                return new IteradorOrdenador(this);
           }
        //un metodo public que nos devuelve un objeto de tipo iteratoordenador que viene siendo 
        //la clase que vamosa consumir desde el program.
       }
}
