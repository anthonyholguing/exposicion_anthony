//****LIBRERIA****
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//PROYECTO
namespace EXPOSICION_ANTHONY
{
    class Program
    {
        static void Main(string[] args)
        {
            Ordenador lista = new Ordenador();
            lista.añadirValor("Elemento 1");
            lista.añadirValor("Elemento 2");
            lista.añadirValor("Elemento 3");
            lista.añadirValor("Elemento 4");
            IteradorOrdenador iterador = lista.iterador();
            //hemos creado un objeto tipo ordenador  al que hemos ido añadiendo elemento
            //en este caso el nombre del elemento ordenador de tal manera que tenemos
            //un arraylist de cuatro elementos

            //RECORRIDO CON EL ITERADOR
            while (iterador.existeSiguiente())
                Console.WriteLine(iterador.siguiente());
            Console.ReadLine();
            //en el metodo existe siguiente nos valida, si existe el siguiente
            //pues lo va imprimiendo por pantalla y asi hasta que acabe
        }

    }
}
